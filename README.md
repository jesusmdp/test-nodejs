# Test practico NodeJS

- [] Crear un servidor web con Nodejs y Express escuchando en el puerto 3001
- [] Crear ruta POST /item que recibirá un JSON de la forma {"item": "foo"} y lo guardará en un array
- [] Crear ruta GET /item que mostrará todos los items guardados en el array
- [] Crear ruta POST /compute que recibirá de body un JSON de la forma {"word1": "foo", "word2": "bar"} y responderá la cadena más larga común entre ambas palabras
